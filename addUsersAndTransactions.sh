#/bin/bash 

network_name=$(cat connection-profile.json | grep \"name\"\: |     tr -d ' ' | cut -d ":" -f 2 | cut -d "\"" -f 2)

#create .bna file from directory with application definition
composer archive create -t dir -n bank/
echo "INSTALL NETWORK" 
composer network install --card PeerAdmin@$network_name-org1 --archiveFile bank@0.0.1.bna

composer network install --card PeerAdmin@$network_name-org2 --archiveFile bank@0.0.1.bna
echo "CREATE ENDORSEMENT POLICY"
#create endorsement policy
cp endorsement-policy.json /tmp/composer/$network_name/endorsement-policy.json

#create admin users
composer identity request -c PeerAdmin@byfn-network-org1 -u admin -s adminpw -d alice

composer identity request -c PeerAdmin@byfn-network-org2 -u admin -s adminpw -d bob

composer network start -c PeerAdmin@byfn-network-org1 -n bank -V 0.0.1 -o endorsementPolicyFile=/tmp/composer/$network_name/endorsement-policy.json -A alice -C alice/admin-pub.pem -A bob -C bob/admin-pub.pem
composer card create -p /tmp/composer/$network_name/org1/$network_name-org1.json -u alice -n bank -c alice/admin-pub.pem -k alice/admin-priv.pem
composer card import -f alice@bank.card
composer network ping -c alice@bank 

composer card create -p /tmp/composer/$network_name/org2/$network_name-org2.json -u bob -n bank -c bob/admin-pub.pem -k bob/admin-priv.pem
composer card import -f bob@bank.card
composer network ping -c bob@bank


##CREATE PARTICIPANTS
composer participant add -c alice@bank -d '{"$class":"org.example.com.SampleClient","clientId":"1", "firstName":"Jozef","lastName":"Mrkvicka"}'
composer identity issue -c alice@bank -f jomr.card -u jomr -a "resource:org.example.com.SampleClient#1"
composer card import -f jomr.card

composer participant add -c alice@bank -d '{"$class":"org.example.com.SampleClient","clientId":"2", "firstName":"Peter","lastName":"Hruska"}'
composer identity issue -c alice@bank -f pehr.card -u pehr -a "resource:org.example.com.SampleClient#2"
composer card import -f pehr.card

composer participant add -c bob@bank -d '{"$class":"org.example.com.SampleClient","clientId":"3", "firstName":"Martin","lastName":"Hruska"}'
composer identity issue -c bob@bank -f mahr.card -u mahr -a "resource:org.example.com.SampleClient#3"
composer card import -f mahr.card

composer participant add -c bob@bank -d '{"$class":"org.example.com.SampleClient","clientId":"4", "firstName":"Pavol","lastName":"Hruska"}'
composer identity issue -c bob@bank -f pahr.card -u pahr -a "resource:org.example.com.SampleClient#4"
composer card import -f pahr.card

composer participant add -c bob@bank -d '{"$class":"org.example.com.SampleClient","clientId":"5", "firstName":"Jan","lastName":"Hruska"}'
composer identity issue -c bob@bank -f jahr.card -u jahr -a "resource:org.example.com.SampleClient#5"
composer card import -f jahr.card

#CREATE ACCOUNTS

composer transaction submit --card jomr@bank -d '{"$class": "org.hyperledger.composer.system.AddAsset", "targetRegistry" : "resource:org.hyperledger.composer.system.AssetRegistry#org.example.com.SampleAccount", "resources": [{"$class": "org.example.com.SampleAccount","accountId":"1","owner":"resource:org.example.com.SampleClient#1","value":"0"}]}'
composer transaction submit --card pehr@bank -d '{"$class": "org.hyperledger.composer.system.AddAsset", "targetRegistry" : "resource:org.hyperledger.composer.system.AssetRegistry#org.example.com.SampleAccount", "resources": [{"$class": "org.example.com.SampleAccount","accountId":"2","owner":"resource:org.example.com.SampleClient#2","value":"0"}]}'
composer transaction submit --card mahr@bank -d '{"$class": "org.hyperledger.composer.system.AddAsset", "targetRegistry" : "resource:org.hyperledger.composer.system.AssetRegistry#org.example.com.SampleAccount", "resources": [{"$class": "org.example.com.SampleAccount","accountId":"3","owner":"resource:org.example.com.SampleClient#3","value":"100"}]}'

##Deposit test
composer transaction submit --card mahr@bank -d '{"$class": "org.example.com.Deposit","account": "resource:org.example.com.SampleAccount#3","value":"100"}'

##withdrawal test
composer transaction submit --card mahr@bank -d '{"$class": "org.example.com.Withdrawal","account": "resource:org.example.com.SampleAccount#3","value":"50"}'
## transfer test
composer transaction submit --card mahr@bank -d '{"$class": "org.example.com.Transfer","srcAccount": "resource:org.example.com.SampleAccount#3","dstAccount": "resource:org.example.com.SampleAccount#1","value":"120"}'
