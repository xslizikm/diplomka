#!/bin/bash
##################################
## initialization script for network run 
## script consists of 3 steps
## 1. create channel
## 2. all peers join channel
## 3. update anchor peers for every organization
##################################
parse_yaml() {
    local prefix=$2
    local s
    local w
    local fs
    s='[[:space:]]*'
    w='[a-zA-Z0-9_]*'
    fs="$(echo @|tr @ '\034')"
    sed -ne "s|^\($s\)\($w\)$s:$s\"\(.*\)\"$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s[:-]$s\(.*\)$s\$|\1$fs\2$fs\3|p" "$1"     |
    awk -F"$fs" '{
    indent = length($1)/2;
    vname[indent] = $2;
    for (i in vname) {if (i > indent) {delete vname[i]}}
        if (length($3) > 0) {
            vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
            printf("%s%s%s=(\"%s\")\n", "'"$prefix"'",vn, $2, $3);
        }
    }' | sed 's/_=/+=/g'
}
# verify the result of the end-to-end test
verifyResult () {
	if [ $1 -ne 0 ] ; then
		echo "!!!!!!!!!!!!!!! "$2" !!!!!!!!!!!!!!!!"
    echo "========= ERROR !!! FAILED to execute init script"
		echo
   		exit 1
	fi
}
echo
echo " ____    _____      _      ____    _____ "
echo "/ ___|  |_   _|    / \    |  _ \  |_   _|"
echo "\___ \    | |     / _ \   | |_) |   | |  "
echo " ___) |   | |    / ___ \  |  _ <    | |  "
echo "|____/    |_|   /_/   \_\ |_| \_\   |_|  "
echo
echo "Initialization of channel"
echo
CHANNEL_NAME="$1"
DELAY="$2"
: ${CHANNEL_NAME:="mychannel"}
: ${TIMEOUT:="60"}
ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
CRYPTO_PATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto-config.yaml
CONFIGTX_PATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/configtx.yaml


echo "Channel name : "$CHANNEL_NAME

count=($(parse_yaml $CRYPTO_PATH | grep Template | cut -d "\"" -f 2))
orgs=($(parse_yaml $CONFIGTX_PATH |  grep "Organizations____ID" | cut -d "\"" -f 2 | grep . | uniq))
#get names of organizations and orderer
org_name=($(cat $CRYPTO_PATH | tr -d ' ' | grep -E "^\-Name" | cut -d ":" -f 2))
 
 #get domains of organizations and orderer
domain_name=($(cat $CRYPTO_PATH | tr -d ' ' | grep -E "^Domain" | cut -d ":" -f 2))

createChannel() {

    if [ ${org_name[i]} == "Orderer" ];
    then
        # create channel
        if [ -z "$CORE_PEER_TLS_ENABLED" -o "$CORE_PEER_TLS_ENABLED" = "false" ]; then
            peer channel create -o orderer.${domain_name[i]}:7050 -c $CHANNEL_NAME -f ./channel-artifacts/channel.tx >&log.txt
        else
            peer channel create -o orderer.${domain_name[i]}:7050 -c $CHANNEL_NAME -f ./channel-artifacts/channel.tx --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA >&log.txt
        
        fi
    res=$?
	cat log.txt
	verifyResult $res "Channel creation failed"
	echo "===================== Channel "$CHANNEL_NAME" is created successfully ===================== "
    fi
}

joinChannel() {

    if [ ${org_name[i]} != "Orderer" ];
    then
        for ((k=0;k<${count[$i-1]};k++))
        do     
            CORE_PEER_LOCALMSPID="${orgs[i]}"
            CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/${domain_name[i]}/peers/peer$k.${domain_name[i]}/tls/ca.crt
	        CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/${domain_name[i]}/users/Admin@${domain_name[i]}/msp
            CORE_PEER_ADDRESS=peer$k.${domain_name[i]}:7051

            #join channel
            peer channel join -b $CHANNEL_NAME.block  >&log.txt
            #Verify result
            res=$?
	        cat log.txt
            verifyResult $res " $CORE_PEER_ADDRESS has failed to Join the Channel"
            echo "===================== $CORE_PEER_ADDRESS joined on the channel \"$CHANNEL_NAME\" ===================== "
            sleep $DELAY
        done
    fi
}

updateAnchorPeers() {

if [ ${org_name[i]} != "Orderer" ];
    then
    
        if [ -z "$CORE_PEER_TLS_ENABLED" -o "$CORE_PEER_TLS_ENABLED" = "false" ]; then
		peer channel update -o orderer.example.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/${orgs[i]}anchors.tx >&log.txt
	else
		peer channel update -o orderer.example.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/${orgs[i]}anchors.tx --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA >&log.txt
        
    fi 
  	res=$?
	cat log.txt
	verifyResult $res "Anchor peer update failed"
	echo "===================== Anchor peers for org "$CORE_PEER_LOCALMSPID" on "$CHANNEL_NAME" is updated successfully ===================== "
	sleep $DELAY

fi
}

for ((i=0;i<${#org_name[@]};i++))
do
createChannel
joinChannel    
updateAnchorPeers
done


echo
echo "========= All GOOD, init completed ==========="
echo

echo
echo " _____   _   _   ____   "
echo "| ____| | \ | | |  _ \  "
echo "|  _|   |  \| | | | | | "
echo "| |___  | |\  | | |_| | "
echo "|_____| |_| \_| |____/  "
echo
exit

