/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';
/**
 * Write your transction processor functions here
 */

/**
 * Sample transaction
 * @param {org.example.com.Deposit} Deposit
 * @transaction
 */

async function Deposit(tx) {
    // Save the old value of the asset.
    const oldValue = tx.account.value;

    // Update the asset with the new value.
    tx.account.value = Number(oldValue)+Number(tx.value);

    // Get the asset registry for the asset.
    const accountRegistry = await getAssetRegistry('org.example.com.SampleAccount');
    // Update the asset in the asset registry.
    await accountRegistry.update(tx.account);

    // Emit an event for the modified asset.
    let event = getFactory().newEvent('org.example.com', 'SampleEvent');
    event.account = tx.account;
    event.oldValue = oldValue;
    event.newValue = Number(oldValue)+Number(tx.value);
    emit(event);
}
/**
 * Sample transaction
 * @param {org.example.com.Withdrawal} Withdrawal
 * @transaction
 */

async function Withdrawal(tx) {
    // Save the old value of the asset.
    const oldValue = tx.account.value;

    // Update the asset with the new value.
    tx.account.value = Number(oldValue)-Number(tx.value);

    // Get the asset registry for the asset.
    const accountRegistry = await getAssetRegistry('org.example.com.SampleAccount');
    // Update the asset in the asset registry.
    await accountRegistry.update(tx.account);

    // Emit an event for the modified asset.
    let event = getFactory().newEvent('org.example.com', 'SampleEvent');
    event.account = tx.account;
    event.oldValue = oldValue;
    event.newValue = Number(oldValue)-Number(tx.value);
    emit(event);
}

/**
 * Sample transaction
 * @param {org.example.com.Transfer} Transfer
 * @transaction
 */

async function Transfer(tx) {
    // Save the old value of the asset.
    const oldValue = tx.srcAccount.value;

    // Update the asset with the new value.
    tx.srcAccount.value = Number(tx.srcAccount.value)-Number(tx.value);
    tx.dstAccount.value = Number(tx.dstAccount.value)+Number(tx.value);
    // Get the asset registry for the asset.
    const accountRegistry = await getAssetRegistry('org.example.com.SampleAccount');
    // Update the asset in the asset registry.
    await accountRegistry.update(tx.srcAccount);
    await accountRegistry.update(tx.dstAccount);
    // Emit an event for the modified asset.
    let event = getFactory().newEvent('org.example.com', 'SampleEvent');
    event.account = tx.srcAccount;
    event.oldValue = oldValue;
    event.newValue = Number(oldValue)-Number(tx.value);
    emit(event);
}


