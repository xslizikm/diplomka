Hyperledger Fabric + Composer example
=====================================

Cieľ : Vytvorenie univerzálnych skriptov, ktoré umožnia na základe definícii siete jej spustenie a prípravu pre nasadenie aplikácie vytvorenej cez Composer
  
    
Dependencies - npm, composer-cli@v0.19.9, composer-rest-server@v0.19.9, composer-playground@0.19.9. Hyperledger Composer závislosti musia byť nainštalované globálne: `$ npm install -g <package-names>`

Momentalne funkcne s verziou composeru v0.19.9 pretoze dalsia verzia je nekompatibilna

Skripty:  
========
*install.sh* - skript plni 2 úlohy  
\- stiahnutie a inštalácia Fabric bináriek + docker images  
\- inštalácia npm balíkov potrebných pre vytváranú aplikáciu, definovanych v súbore package.json v adresári obsahujúcom definície aplikácie
\- skript potrebuje ako argument názov adresáru v ktorom sa nachádzajú definície aplikácie, v tomto prípade `$ ./install.sh bank`  
  
*managenetwork.sh* - skript slúži na generovanie certifikátov a klúčov a manipuláciu s Fabric sieťou, čiže ju dokáže spustiť, zastaviť a reštartovať
\- spustenie - `$ ./managenetwork.sh -m up`  
\- zastavenie - `$ ./managenetwork.sh -m down`  
\- reštart - `$ ./managenetwork.sh -m restart`  
  
*scripts/script.sh* - skript, ktorý sa vloži do docker CLI kontajneru pri spustení siete a vytvorí kanál, ku ktorému pripojí všetkých peers definovaných vytvorených v *crypto-config.yaml*  
  
*deployscript.sh* - skript skopiruje certifikáty a klúče na správne miesta a vytvorí administrátora pre každú organizáciu, ktorá v sieti vystupuje  
  
*addUsersAndTransactions.sh* - skript špecifický pre tento príklad, vytvorí identitu adminom a používateľom + vykoná každý typ transakcie  
  
  
Ostatné súbory:
===============  
  
Ide o súbory, ktoré je potrebné vytvoriť špecificky podľa toho akú fabric sieť chceme vytvoriť.    
  
*configtx.yaml* - obsahuje informácie o organizáciach, peer uzloch a konfiguračných profiloch  
  
*crypto-config.yaml* - obsahuje definície organizácii  
Parsovaním súborov *configtx.yaml* a *crypto-config.yaml* sa získavajú informácie potrebné pre vytvorenie univerzálnych skriptov  
  
*connection-profile.json* - obsahuje informácie, ktoré potrebuje Composer aby sa pripojil na Fabric network  
  
*endorsement-policy.json* - obsahuje informáciu o počte potrebných suhlasov na schvalenie transakcie  
  
Zvyšný obsah adresára pozostava z compose files



