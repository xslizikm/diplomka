#!/bin/bash

############
# check if hyperledger composer dependencies are installed.
############
function checkDeps() {
    echo "composer cli version is: "
    composer --version
    if [ "$?" -ne 0 ]; then
        echo "Hyperledger composer-cli is not installed..."
        exit 1
    fi
    echo "composer playground version is: "
    composer-playground --version
    if [ "$?" -ne 0 ]; then
        echo "Hyperledger composer-playground is not installed..."
        exit 1
    fi
    echo "composer rest server version is: " 
    composer-rest-server --version
    if [ "$?" -ne 0 ]; then
        echo "Hyperledger composer-rest-server is not installed..."
        exit 1
    fi
}

##############
# create directory for network configuration files : /tmp/composer/$network_name
# removes directory on reboot - only for testing purposes
##############
function prepDestDir() {

    network_name=$(cat connection-profile.json | grep \"name\"\: | tr -d ' ' | cut -d ":" -f 2 | cut -d "\"" -f 2)
    rm -rf /tmp/composer/$network_name
    mkdir -p /tmp/composer/$network_name
    #copy default connection profile - musi vytvorit pouzivatel manualne
    cp connection-profile.json /tmp/composer/$network_name/$network_name.json

}

##########
# copy generated certs and keys for orderer and orgs into /tmp/composer/$network_name directory
##########
copyCertsAndKeys() {

    #get names of organizations and orderer
    org_name=($(cat crypto-config.yaml | tr -d ' ' | grep -E "^\-Name" | cut -d ":" -f 2))

    #get domains of organizations and orderer
    domain_name=($(cat crypto-config.yaml | tr -d ' ' | grep -E "^Domain" | cut -d ":" -f 2))

    # cycle trough orgs and move their CA certs and keys to network specific directory
    for ((i=0;i<${#org_name[@]};i++))
    do
        lorg_name=$(echo "${org_name[i]}" | tr '[:upper:]' '[:lower:]')
        if [ "$i" -eq "0" ];
        then
            #get orderer CA cert
            awk 'NF {sub(/\r/, ""); printf "%s\\\\n",$0;}' crypto-config/ordererOrganizations/"${domain_name[i]}"/orderers/$lorg_name."${domain_name[i]}"/tls/ca.crt > /tmp/composer/$network_name/ca-$lorg_name.txt
            replace_orderer="INSERT_ORDERER_CA_CERT"
            pk_orderer=$(</tmp/composer/$network_name/ca-$lorg_name.txt)
            awk 'NF { gsub(/'"$replace_orderer"'/,"'"$pk_orderer"'" ); print }' /tmp/composer/$network_name/$network_name.json > /tmp/composer/$network_name/temp.json && mv /tmp/composer/$network_name/temp.json /tmp/composer/$network_name/$network_name.json

        else
            mkdir -p /tmp/composer/$network_name/$lorg_name
            #get peer orgs CA certs
            awk 'NF {sub(/\r/, ""); printf "%s\\\\n",$0;}' crypto-config/peerOrganizations/"${domain_name[i]}"/peers/peer0."${domain_name[i]}"/tls/ca.crt > /tmp/composer/$network_name/$lorg_name/ca-$lorg_name.txt
            #add orgs CA CERTS to connection profiles
            pk_org=$(</tmp/composer/$network_name/$lorg_name/ca-$lorg_name.txt)
            replace_str=$(echo $lorg_name | tr [a-z] [A-Z])
            replace_str=$(echo INSERT_"$replace_str"_CA_CERT)
            awk 'NF { gsub(/'"$replace_str"'/,"'"$pk_org"'"); print}' /tmp/composer/$network_name/$network_name.json > /tmp/composer/$network_name/temp.json && mv /tmp/composer/$network_name/temp.json /tmp/composer/$network_name/$network_name.json
            ##copy generated private keys
            export ORG=crypto-config/peerOrganizations/"${domain_name[i]}"/users/Admin@"${domain_name[i]}"/msp
            cp -p $ORG/signcerts/A*.pem /tmp/composer/$network_name/$lorg_name
            cp -p $ORG/keystore/*_sk /tmp/composer/$network_name/$lorg_name

        fi
done
}

##############
# create admin identity for every org
##############
createOrgAdmins() {

    for ((i=1;i<${#org_name[@]};i++))
    do
        lorg_name=$(echo "${org_name[i]}" | tr '[:upper:]' '[:lower:]')

        #customize connection profile for orgs
        cp /tmp/composer/$network_name/$network_name.json /tmp/composer/$network_name/$lorg_name/$network_name-$lorg_name.json
            client_info=$(echo '    "client": {
                "organization": "INSERT",
                "connection": {
                    "timeout": {
                        "peer": {
                            "endorser": "300",
                            "eventHub": "300",
                            "eventReg": "300"
                        },
                        "orderer": "300"
                    }
                }
            },')
    
        customization=$(echo "$client_info" | sed "s|INSERT|"${lorg_name^}"|g")
        awk -v line="4" -v text="$customization" '{print} NR==line{print text}' /tmp/composer/$network_name/$lorg_name/$network_name-$lorg_name.json > /tmp/composer/$network_name/tmp.file && mv /tmp/composer/$network_name/tmp.file  /tmp/composer/$network_name/$lorg_name/$network_name-$lorg_name.json

        echo "GENERATING BUSINESS NETWORK CARDS FOR ADMIN"
        echo "$lorg_name"
        echo "${domain_name[i]}"
        ##generate admin's business network card for every org
        composer card create -p /tmp/composer/$network_name/$lorg_name/$network_name-$lorg_name.json -u PeerAdmin -c /tmp/composer/$network_name/$lorg_name/Admin@"${domain_name[i]}"-cert.pem -k /tmp/composer/$network_name/$lorg_name/*_sk -r PeerAdmin -r ChannelAdmin -f PeerAdmin@$network_name-$lorg_name.card

        ##import business cards into wallet
        composer card import -f PeerAdmin@$network_name-$lorg_name.card --card PeerAdmin@$network_name-$lorg_name

done
}

#set current directory to directory of script
cd "$(dirname "$0")"
###ONLY FOR DEVELOPMENT PURPOSES###
composer card delete -c PeerAdmin@byfn-network-org1
composer card delete -c PeerAdmin@byfn-network-org2
composer card delete -c alice@bank
composer card delete -c bob@bank
composer card delete -c jomr@bank
composer card delete -c jahr@bank
composer card delete -c pahr@bank
composer card delete -c pehr@bank
composer card delete -c mahr@bank
####################################


checkDeps
prepDestDir
copyCertsAndKeys
createOrgAdmins


